/* 
 * File:   main.c
 * Author: Juan Felipe
 *
 * Created on 3 de julio de 2019, 04:55 PM
 */

#include <stdio.h>
#include <stdlib.h>
#include "plib.h"

void main(void) {
    mPORTESetPinsDigitalOut(BIT_4);
    mPORTESetBits(BIT_4);
    while(1){}
}